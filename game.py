from random import randint

name = input("Hi! What is your name? ")

for nums in range(4):
    month_guessed = randint(1, 12)
    year_guessed = randint(1924, 2004)
    print("Guess {0}: {1} were you born in {2} / {3}?".format(
        nums + 1, name, month_guessed, year_guessed))
    answer = input("yes or no? ").lower()
    if answer == "yes":
        print("I knew it!")
        break
    elif answer == "no" and nums + 1 == 4:
        print("I have other things to do. Good bye.")
    else:
        print("Drat! Lemme try again!")
